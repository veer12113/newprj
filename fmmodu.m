
clc;
clear all;
close all;
[x,fs] = audioread('song.wav');

 T=1/fs;
 L=length(x);
 t = (0:L-1)*T;
fc = 100000000; 
fs1=2*fc;
% f=0:fs1:5;
% sound(x,fs);
%  t = 0:1/fs:2;

fDev = 75000;
data = fmmod(x,fc,fs1,fDev);
datademod = fmdemod(data,fc,fs1,fDev);
figure;
subplot(2,1,2);
 sound(100*datademod,fs);
% xfft=fft(x);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(x,NFFT)/L;
f = fs/2*linspace(0,1,NFFT/2+1);

% Plot single-sided amplitude spectrum.
plot(f,2*abs(Y(1:NFFT/2+1)));
title('Spectrum of original signal');
xlabel('f(hz)');
ylabel('amplitude');
subplot(2,1,1);
l1=length(datademod);
NFFT1 = 2^nextpow2(l1); % Next power of 2 from length of y
Y1 = fft(datademod*100,NFFT1)/l1;


f2 = fs/2*linspace(0,1,NFFT1/2+1);

% Plot single-sided amplitude spectrum.
plot(f2,2*abs(Y1(1:NFFT1/2+1)));

title('Spectrum of demodulated signal');
xlabel('f(hz)');
ylabel('amplitude');
figure;
l2=length(data);
NFFT2 = 2^nextpow2(l2); % Next power of 2 from length of y
Y2 = fft(data,NFFT2)/l2;

f3 = fs1/2*linspace(0,1,NFFT2/2+1);

% Plot single-sided amplitude spectrum.
plot(f3,2*abs(Y2(1:NFFT2/2+1)));
xlim([150000 200000]);
title('Spectrum of FM Modulated signal');
xlabel('f(hz)');
ylabel('amplitude');
% datafft= fft(data);
% demodfft= fft(datademod);
% subplot(2,1,1);
% plot(demodfft);
% subplot(2,1,2);
% plot(xfft);



% plot(t,data);
% subplot(3,1,1);
% stem(x);
% xlabel('time');
% ylabel('amplitude');
% subplot(3,1,2);
% plotata);
% xlabel('time');
% ylabel('amplitude');
% subplot(3,1,3);
% plot(t,datademod);
% xlabel('time');
% ylabel('amplitude');
% xlabel('time');
% ylabel('amplitude');
% % x = sin(2*pi*30*t)+2*sin(2*pi*60*t);
% % plot(t,x,'c',t,z,'b--');
% % xlabel('Time (s)')
% % ylabel('Amplitude')
% % legend('Original Signal','Demodulated Signal')
